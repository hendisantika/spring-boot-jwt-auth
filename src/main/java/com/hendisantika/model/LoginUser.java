package com.hendisantika.model;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jwt-auth
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 09.38
 * To change this template use File | Settings | File Templates.
 */

@Data
public class LoginUser {
    private String username;
    private String password;
}

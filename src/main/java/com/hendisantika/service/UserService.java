package com.hendisantika.service;

import com.hendisantika.model.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jwt-auth
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 09.45
 * To change this template use File | Settings | File Templates.
 */

public interface UserService {

    User save(User user);

    List<User> findAll();

    void delete(long id);

    User findOne(String username);

    User findById(Long id);
}

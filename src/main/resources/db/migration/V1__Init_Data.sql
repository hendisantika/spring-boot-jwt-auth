CREATE TABLE User (
id BIGINT NOT NULL PRIMARY KEY ,
username VARCHAR (25) NOT NULL ,
password VARCHAR (75) NOT  NULL ,
salary BIGINT (10) NOT NULL ,
age INT (2) NOT NULL
);


INSERT INTO User (id, username, password, salary, age) VALUES (1, 'hendi', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 3456, 33);
INSERT INTO User (id, username, password, salary, age) VALUES (2, 'naruto', '$2a$04$PCIX2hYrve38M7eOcqAbCO9UqjYg7gfFNpKsinAxh99nms9e.8HwK', 7823, 23);
INSERT INTO User (id, username, password, salary, age) VALUES (3, 'sasuke', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 4234, 45);
INSERT INTO User (id, username, password, salary, age) VALUES (4, 'sakura', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 4234, 45);
